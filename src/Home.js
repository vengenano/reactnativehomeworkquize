import React from "react";
import { View, Text } from "react-native";

import { Container, Header, Content, Button } from 'native-base';
export default class Home extends React.Component {
  render() {
    return (
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Text style={{fontSize:25}}>Please Happy with App Quize </Text>
        <Button success style={{padding:20,marginLeft:60,marginTop:30}} onPress={()=>this.props.navigation.navigate('Homework')} ><Text style={{fontSize:30}}> Go to Play Game </Text></Button>
        </View>
    );
  }
}

import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { Container, Content, Card, CardItem, Button, Text } from 'native-base'
export default class componentName extends Component {
    static navigationOptions = {   
        title:"Please Choose the best answer"
    };
    state = {

        questions: [
            {
                text: "What is the name of the US president?",
                correct: 1,
                answers: [
                    "Obama",
                    "Trump",
                    "Roosvelt",
                    "Putin"
                ]
            },
            {
                text: "What is the square root of 100?",
                correct: 3,
                answers: [
                    "sin(10)",
                    "1",
                    "100",
                    "10"
                ]
            },
            {
                text: "Who is develop React Native and js",
                correct: 0,
                answers: [
                    "FaceBook developer Team",
                    "Yourtube developer Team",
                    "Google developer Team",
                    "None of All"
                ]
            },
            {
                text: "Who is the president in Khmer",
                correct: 0,
                answers: [
                    "Hun Sen",
                    "Kemarak",
                    "Jerm",
                    "None of All"
                ]
            }
        ],
        score: 0,
        result: []
    }
    onSelect(indA, indQ) {
        let selected = this.state.result
        selected[indQ] = indA
        this.setState({
            result: selected
        })
        console.log('Selected[]==>', selected)
        let newScore = 0;
        for (let ind in this.state.result) {

            if (this.state.questions[ind].correct == this.state.result[ind]) {
                newScore++;
                var numberOfQuestion=this.state.questions.length
                if(newScore==numberOfQuestion){
                    this.setState({
                        score:((newScore/numberOfQuestion*100).toFixed(2))
                    })
                   
                }
                else{   
                    this.setState({
                        score:((newScore/numberOfQuestion*100).toFixed(2))
                    })
                }
                
            }
        }
    }
    componentWillMount(){
        console.log('num',this.state.questions.length)
    }
    render() {
        return (
            <Container>
                   <Content padder>
                    <Card>
                        {this.state.questions.map((question, indQ) => (
                            <View key={indQ}>
                                <Text>{question.text}</Text>
                                <RadioGroup
                                    size={24}
                                    thickness={2}
                                    color='#9575b2'
                                    onSelect={(indA) => this.onSelect(indA, indQ)}>
                                    {
                                        question.answers.map(answer => (
                                            <RadioButton value={answer} key={answer} >
                                                <Text>{answer}</Text>
                                            </RadioButton>
                                        ))
                                    }
                                </RadioGroup>
                            </View>
                        ))}
                        <Button style={{ alignSelf: 'center', marginBottom: 20 }} success onPress={() => this.props.navigation.navigate('Result', this.state.score)}>
                            <Text>Submit</Text>
                        </Button>
                    </Card>
                </Content>

            </Container>
        )
    }
}
import React from "react";
import { View, Text } from "react-native";

import { Container, Header, Content, Button, Icon } from 'native-base';
export default class Result extends React.Component {

    static navigationOptions = {
        title: "Back"
       
    };
    render() {
        console.log('==>', this.props)
        return (
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                <Text style={{ fontSize: 25 }}>Thsnk for your play Game</Text>
                <Text style={{ fontSize: 30 }}>Your Score : {this.props.navigation.state.params} </Text>
                <Button style={{padding:20,alignSelf:'center'}}
                    onPress={() => this.props.navigation.navigate('Homework')}>
                    <Text>Home</Text>
                </Button>
            </View>
        );
    }
}

import {createStackNavigator,createAppContainer} from 'react-navigation';
import Home from './Home';
import Result from './Result';
import Homework from './Homework';
const AppNavigator=createStackNavigator(
    {
        Home:Home,
        Result:Result,
        Homework:Homework
    },{
        initialRouteName: 'Home',
        /* The header config from HomeScreen is now here */
        defaultNavigationOptions: {
          headerStyle: {
            backgroundColor: '#f4511e',
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        },
      }
    );
export default createAppContainer(AppNavigator)

